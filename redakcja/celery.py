from __future__ import absolute_import

import os
import sys

ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path = [
    os.path.join(ROOT, 'apps'),
] + sys.path

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'redakcja.settings')

from celery import Celery
from django.conf import settings

app = Celery('redakcja')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
