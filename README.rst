====================
Platforma Redakcyjna
====================

Opis projektu
=============
Platforma to serwis służący do grupowej edycji książek na potrzeby serwisu WolneLektury.pl.

Instalacja i użycie
===================

#. Zainstaluj zależności dla `Pillow <https://pillow.readthedocs.io/en/latest/installation.html#building-on-linux>`_ i `lxml <http://lxml.de/installation.html#requirements>`_
#. Zainstaluj wymagane biblioteki komendą::

	pip install -r requirements.txt

#. Pobierz pakiet `librarian` jako submoduł::

	git submodule update --init

#. Skopiuj zawartość pliku `redakcja/localsettings.sample` do `redakcja/localsettings.py` i zmień go zgodnie ze swoimi potrzebami.

#. Wypełnij bazę danych (Django poprosi o utworzenie pierwszego użytkownika – nie rób tego)::

	./manage.py syncdb
	./manage.py migrate

#. Uruchom serwer deweloperski::

	./manage.py runserver


Testy
=====

Python::

    $ pip install -r requirements-test.txt
    $ ./manage.py test --settings=redakcja.settings.test

JavaScript (wymagany node.js i xsltproc)::

    $ npm install
    $ ./node_modules/.bin/mocha -u tdd $(find -name *_test.js)

