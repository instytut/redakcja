-i https://py.mdrn.pl:8443/simple

## Python libraries
lxml>=2.2.2
Mercurial>=3.3,<3.4
PyYAML>=3.0
Pillow
oauth2
httplib2 # oauth2 dependency

## Book conversion library
https://gitlab.com/instytut/librarian/repository/47f714f5e0e93547ddce048d9ec7abd9b0ab8ee3/archive.tar.gz#egg=librarian==1.7

## Django
Django>=1.11,<1.12
fnpdjango>=0.2,<0.3
django-pipeline>=1.6.9,<1.7
django_cas>=2.1,<2.2
sorl-thumbnail>=12.2,<13
django-maintenancemode>=0.9
dj-pagination
django-gravatar2
python-slugify

celery>=3.1.12,<3.2
kombu>=3.0,<3.1

raven
argparse
