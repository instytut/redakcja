# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import RedirectView


urlpatterns = []

if getattr(settings, 'CAS_SERVER_URL', None):
    from django_cas.views import login, logout
    urlpatterns += [
        # Auth
        url(r'^accounts/login/$', login, name='login'),
        url(r'^accounts/logout/$', logout, name='logout'),
        url(r'^admin/login/$', login, name='login'),
        url(r'^admin/logout/$', logout, name='logout'),
    ]
else:
    from django.contrib.auth.views import login, logout
    urlpatterns += [
        url(r'^accounts/login/$', login, name='login'),
        url(r'^accounts/logout/$', logout, name='logout'),
    ]

urlpatterns += [
    # Admin panel
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', RedirectView.as_view(url= '/documents/', permanent=False)),
    url(r'^documents/', include('catalogue.urls')),
    url(r'^apiclient/', include('apiclient.urls')),
    url(r'^editor/', include('wiki.urls')),
    url(r'^images/', include('wiki_img.urls')),
    url(r'^cover/', include('cover.urls')),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
