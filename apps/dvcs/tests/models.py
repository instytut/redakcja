from dvcs.models import Document


class ADocument(Document):
    class Meta:
        app_label = 'dvcs'
