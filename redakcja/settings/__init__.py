from __future__ import absolute_import
import os.path
from redakcja.settings.common import *


try:
    LOGGING_CONFIG_FILE
except NameError:
    LOGGING_CONFIG_FILE = os.path.join(PROJECT_ROOT, 'config',
                                ('logging.cfg' if not DEBUG else 'logging.cfg.dev'))
try:
    import logging

    if os.path.isfile(LOGGING_CONFIG_FILE):
        import logging.config
        logging.config.fileConfig(LOGGING_CONFIG_FILE)
    else:
        import sys
        logging.basicConfig(stream=sys.stderr)
except (ImportError,), exc:
    raise
