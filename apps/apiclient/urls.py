from django.conf.urls import url
from apiclient import views


urlpatterns = [
    url(r'^oauth/$', views.oauth, name='apiclient_oauth'),
    url(r'^oauth_callback/$', views.oauth_callback, name='apiclient_oauth_callback'),
]
