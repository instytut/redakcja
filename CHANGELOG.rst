==========
Change Log
==========

This document records all notable changes to Redakcja.


1.2 (2018-03-28)

* Move to Django 1.11 (last Django on Python 2.7).
* Removed South migrations, added native Django migrations. For existing
  installations, the initial migrations will have to be faked.
* Move librarian from submodule to normal package.
* Removed old unused `assign_from_redmine` command.
* Removed comments - they were already disabled anyway.
* Replace unmaintained `django-pagination` with `dj-pagination`.
* Upgrade `django-pipeline` to current version.


1.1 (2018-03-21)
----------------

* Allow running without CAS.


1.0 (2018-03-20)
----------------

* Added the version number and change log.
